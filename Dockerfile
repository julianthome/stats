FROM pandoc/core

RUN apk update \
    && apk add --no-cache git bash R R-dev ruby ruby-dev git-lfs sqlite-dev \
    build-base bash py3-pip msttcorefonts-installer fontconfig \
    && update-ms-fonts && fc-cache -f


RUN R -e "install.packages('rmarkdown', repos='https://cran.rstudio.com')"
RUN R -e "install.packages('ggplot', repos='https://cran.rstudio.com')"
RUN R -e "install.packages('ggplot2', repos='https://cran.rstudio.com')"
RUN R -e "install.packages('RSQLite', repos='https://cran.rstudio.com')"

ENTRYPOINT [""]
CMD ["/bin/bash"]
